document.addEventListener('DOMContentLoaded', () => {
    let button = document.querySelector('.change');
    let bodyClass = document.querySelector('body');


    let themeByDefault = 'theme';
    if (localStorage.getItem('background') !== null) {
        themeByDefault = localStorage.getItem('background');
    }
    document.body.classList.add(themeByDefault);
    button.addEventListener('click', () => {

        bodyClass.classList.toggle('theme');
        bodyClass.classList.toggle('opposite');
        if (bodyClass.classList.contains('theme')) {
            localStorage.setItem('background', 'theme');

        } else if (bodyClass.classList.contains('opposite')) {
            localStorage.setItem('background', 'opposite');

        }


    });
});